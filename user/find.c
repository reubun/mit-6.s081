#include "kernel/types.h"
#include "kernel/stat.h"
#include "kernel/fs.h"
#include "kernel/fcntl.h"
#include "user/user.h"

// 写代码过程中，共出现三个错误
// 如下面代码中注释前的编号
// 编写代码过程中不是很熟练，在回头复习看看,多编码

char*
fmtname(char *path)
{
  static char buf[DIRSIZ+1];
  char *p;

  // Find first character after last slash.
  for(p=path+strlen(path); p >= path && *p != '/'; p--)
    ;
  p++;

  // Return blank-padded name.
  if(strlen(p) >= DIRSIZ)
    return p;
  memmove(buf, p, strlen(p));
  //memset(buf+strlen(p), ' ', DIRSIZ-strlen(p));
  buf[strlen(p)] = 0;  // 3. 字符串结尾，直接ls的函数拿过来用导致查询不到结果，如上面注释的一行，因为那样会导致strcmp不会返回0
  return buf;
}

void find(char* search_path, const char* file_name) {
    int fd;
    struct stat st;
    struct dirent de;
    char buf[512], *p;

    if((fd = open(search_path, O_RDONLY)) < 0) {
        fprintf(2, "find: can not open %s\n", search_path);
        return;
    }
    if(fstat(fd, &st) < 0){
        fprintf(2, "find: cannot stat %s\n", search_path);
        close(fd);
        return;
    }

    if (st.type == T_FILE) {  // 文件
        if (0 == strcmp(fmtname(search_path), file_name)) {  // match
            printf("%s\n", search_path);
        }
    } else if (st.type == T_DIR) {  // 目录
        strcpy(buf, search_path);
        p = buf+strlen(buf);
        *p++ = '/';
        while(read(fd, &de, sizeof(de)) == sizeof(de)){
            if(de.inum == 0  // 2. 这个是有用的？啥意思
                || 0 == strcmp(".", de.name)
                || 0 == strcmp("..", de.name))  // 跳过"."和".."？
                continue;
            memmove(p, de.name, DIRSIZ);
            p[DIRSIZ] = 0;
            find(buf, file_name);
        }
    }
    close(fd);  // 1. 注意这里的close，不加这个时open函数会报错，第33行!是因为xv6对打开文件个数有限制吗？35个？           
}

int
main(int argc, char *argv[])
{
    if(argc < 3){
        fprintf(2, "Usage: find path name\n");
        exit(1);
    }

    find(argv[1], argv[2]);
    exit(0);
}