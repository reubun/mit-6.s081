#include "kernel/types.h"
#include "kernel/stat.h"
#include "user/user.h"

// 错误记录
// 1. fork()的返回值搞混了，>0表示父进程，导致编译后输出错误结果且一直卡着
// 2. 对read(),write()的返回值掌握欠缺。
//    什么时候表示没有数据可读了？
//    什么时候表示读/写出现错误了？
//    若有很多数据且不知道多少次能读完该如何读？
// 3. 与@PKUFlyingPig的代码对比，主要是思维方法的差距，
//    学习他的注释，包括父子进程模块总体的概述，让自己写相应模块代码时对该模块有个清晰的整体逻辑
//    模块内部的注释，尤其是父进程模块中wait()处的注释，为啥要wait写清楚了
//    报错信息的表述更清晰准确，出错时更能准确的判断是哪个部分报错的
int main(int argc, char* argv[]) {
    int p[2];
    int n;
    char* send_buf = "H";
    char rec_buf[1];
    pipe(p);
    int pid = fork();
    if (pid > 0) {
        // parent
        if (write(p[1], send_buf, 1) != 1) {
            fprintf(2, "write error\n");
            exit(1);
        }
        close(p[1]);
        wait(0);

        n = read(p[0], rec_buf, 1);
        if (n == 1) {
            printf("%d: received pong\n", getpid());
        } else if (n < 0) {
            fprintf(2, "read error\n");
            exit(1);
        }
    } else if (pid == 0) {
        // child
        n = read(p[0], rec_buf, 1);
        if (n == 1) {
            printf("%d: received ping\n", getpid());
        } else if (n < 0) {
            fprintf(2, "read error\n");
            exit(1);
        }

        if (write(p[1], rec_buf, 1) != 1) {
            fprintf(2, "write error\n");
            exit(1);
        }
        close(p[0]);
        close(p[1]);
    } else {
        fprintf(2, "fork error\n");
        exit(1);
    }

    exit(0);
}