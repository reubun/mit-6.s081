#include "kernel/types.h"
#include "kernel/stat.h"
#include "user/user.h"

/*
* p = get a number from left neighbor
* print p
* loop:
*    n = get a number from left neighbor
*    if (p does not divide n)
*        send n to right neighbor
*/

/*
// 第一遍写不出来，看了@PKUFlyingPig的代码能大概看懂
int main(int argc, char* argv[]) {
    int n;
    int feeds_pipe[2];
    pipe(feeds_pipe);
    close(feeds_pipe[0]);  // 1
    if (fork() == 0) {
        // 问题：在fork后子进程中管道feeds_pipe读写两端都开着呢吗？
        // fork后子进程复制父进程的内存，是指之前的程序也都执行吗？那么上述"1"处语句会执行吗
        // 所以问题就是fork到底是如何复制父进程的内存空间的？在此之前的代码都会执行一遍吗？
    } else {
        // 其他人的写法是这样，不在上面公共部分写这一行代码
        close(feeds_pipe[0]);
    }
}*/

// The following code copy from @PKUFlyingPig
void new_proc(int p[2]){
	int prime;
	int flag;
	int n;
	close(p[1]);
	if(read(p[0], &prime, 4) != 4){
		fprintf(2, "child process failed to read\n");
		exit(1);
	}
	printf("prime %d\n", prime);

	flag = read(p[0], &n, 4);
	if(flag){  // 注意这里若换成while(read(p[0], &n, 4))的话，子进程就不是递归的创建了，而是横向创建了，那么这样可以吗？
		int newp[2];
		pipe(newp);
		if (fork() == 0)
		{
			new_proc(newp);
		}else
		{
			close(newp[0]);
			if(n%prime)write(newp[1], &n, 4);
			
			while(read(p[0], &n, 4)){
				if(n%prime)write(newp[1], &n, 4);
			}
			close(p[0]);
			close(newp[1]);
			wait(0);
		}
	}
	exit(0);
}

int main(int argc, char const *argv[])
{
	int p[2];
	pipe(p);
	if (fork() == 0)
	{
		new_proc(p);
	}else
	{
		close(p[0]);
		for(int i = 2; i <= 35; i++)
		{
			if (write(p[1], &i, 4) != 4)
			{
				fprintf(2, "first process failed to write %d into the pipe\n", i);
				exit(1);
			}
		}
		close(p[1]);
		wait(0);
		exit(0);
	}
	return 0;
}