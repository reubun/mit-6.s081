#include "kernel/types.h"
#include "kernel/stat.h"
#include "kernel/param.h"

#include "user/user.h"

// 错误：wait使用造成的，不能在循环创建子进程完成后在调用wait
//       需要每次创建子进程，都需要调用wait

// 概述：代码整体思路如下：
//       1. 从标准输入读取参数，每个'\n'表示一个参数
//       2. 对于1中每个参数创建一个子进程，将读到的参数添加到argv最后，执行命令
//          注意在调用exec执行命令时，argv[0]是代表xargs的，所以从argv[1]作为参数执行命令
// 记得整理一下代码，写的好乱

int main(int argc, char* argv[]) {
    char c_tmp, line[512];
    char* new_argv[MAXARG];
    char* ex_argv[MAXARG];
    int id = 0, ex_arg_id = 0;
    while (read(0, &c_tmp, 1) == 1) { // read arguments from stdin
        if (c_tmp != '\n') {
            line[id++] = c_tmp;
            continue;
        } else {
            line[id] = '\0';
            id = 0;
            ex_argv[ex_arg_id++] = line;
        }
    }
    for (int i = 0; i < ex_arg_id; i++) {
        int n_id = 0;
        for (int i = 1; i < argc; i++) {
            new_argv[n_id++] = argv[i];
        }
        new_argv[n_id++] = ex_argv[i];
        new_argv[n_id] = 0;
        if (fork() == 0) {  // child, exec command which has extend arguments from parent
            exec(new_argv[0], new_argv);
            fprintf(2, "exec %s failed\n", argv[0]);
        } else {  // parent, wait
            wait(0);  // 这个wait别忘了，不要写在循环外面，wait只能处理单个子进程
            continue;
        }
    }

    exit(0);
}